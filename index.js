import minimist from 'minimist'
import { dirname } from 'path';
import { fileURLToPath } from 'url';
import fs from 'fs'

const __filename = fileURLToPath(import.meta.url)

const __dirname = dirname(__filename)

const  args = minimist(process.argv.slice(2))

let testFolder = './public/static/dnb/'
const defaultFilters = '2024^2023^2022^2021^2020^2019^2018^2017^2016^2015^2013'

async function createDirectories(filepath, base) {
    const dir = base + filepath.replace('.','')
    console.log('dir:' +  dir)
    return fs.mkdirSync(dir, { recursive: true })
}

function isSymlink(filepath) {
    try {
        return fs.lstatSync(filepath).isSymbolicLink();
    } catch (err) {}
    return null;
}

  
  async function output(file, base) {
    if (file.isDirectory()) {
        await createDirectories(file.path.replace(__dirname,'') + '/' + file.name, base);
    } else{
        const fileoutput = base + file.path.replace(__dirname,'') + '/' + file.name
        console.log('src file: ' + file.path + '/' + file.name)
        if (file.isFile())   fs.copyFileSync(file.path + '/' + file.name, fileoutput);
        console.log('dest file: ' + fileoutput)
        console.log('OK')
    } 
  }

  async function checkFileExists(file) {
    return fs.promises.access(file, fs.constants.F_OK)
             .then(() => true)
             .catch(() => false)
  }

async function exec() {
    try {
        console.log('début export')
        console.log(exec);
        if (args._.length > 1 ) {
            testFolder = args._[1]
        }
        let filters = defaultFilters
        if (args._.length > 0 ) {
            filters = String(args._[0])
        }
        if (filters.includes('all')){
            filters = defaultFilters
        }
        const filtersArray =  filters.split('^')
        console.log('testFolder:'+testFolder)
        console.log('cwd:' + process.cwd())
        console.log('bin:' + __dirname) 

        filtersArray.forEach(filter =>{
            const filterFull = __dirname + '/' + filter
            console.log(filterFull)

            // const filterFull = 'D:\\.pnpm-store\\v3\\tmp\\dlx-9096\\node_modules\\.pnpm\\forge.apps.education.fr+coopmaths+dnb@51d62fc0022e2ae677540472984d1f031dc20342\\node_modules\\dnb-files'+ '/' + filter
            try {
                fs.accessSync(filterFull, fs.F_OK);
                // Do something
                const files = fs.readdirSync(filterFull, {recursive: true, withFileTypes: true})
                files.forEach(file => {
                    output(file, testFolder)
                })
            } catch (e) {
                console.log('does not exist:' + filterFull)
            }      
        })
    } catch (err) {
        console.log(`An error occurred while downloading:`, err);
    }
    console.log('fin export')
}

await exec()
